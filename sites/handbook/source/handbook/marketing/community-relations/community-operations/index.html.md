---
layout: handbook-page-toc
title: "Community Operations"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Our mission

GitLab's Community Operations Program is responsible for developing and maintaining the infrastructure and resources to support the Community Relations team and the GitLab community at large. In this, we help support GitLab's greater mission of 'everyone can contribute' by encouraging the wider GitLab community through process and opportunity.  

### Partnership

The mission of this program is to act as a partner to all Program Managers in the Community Relations team to define, implement and refine KPIs/PIs to measure and report the success and effectiveness of our community programs. We work together with the Community Relations team’s Program Managers to produce regular, engaging content to highlight their programs and attract new contributors. 

The Community Operations function also works closely with the Marketing Operations, and Data and Analytics teams. 

### Tooling

Community Operations defines and maintains the tool stack required to measure and interact with the wider GitLab community.
The Community Program Manager acts as the DRI for the Community Relations team’s webpages on about.gitlab.com. This program also supports the Open Source and Education teams by processing program applications and renewals. Ultimately, we are working towards a process to fully automate this.

### GitLab's Community

The Community Operations Program curates and maintains documentation for any team member to productively engage with the wider community. When necessary, we engage with specialists within GitLab to provide responses and listen to our community’s feedback on [The GitLab forum](forum.gitlab.com), the [GitLab blog](blog.gitlab.com) and Hackernews.

The [Community Operations Manager](https://about.gitlab.com/handbook/marketing/community-relations/#who-we-are) reports to the [Director of the Community Relations Team](https://about.gitlab.com/job-families/marketing/director-of-community-relations/). 

## Community Operations Work

You can find the Community Operations Program peppered throughout the Community Relations handbook, processes, and projects. 

In order to loop in Community Operations on GitLab.com, please use the `community-ops` label. 

Everything labeled `community-ops` is organized on the [Community Operations Issue Board](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/boards/2062229?assignee_username=LindsayOlson&label_name[]=Community%20Ops). 

### How to Use the Community Operations Issue Board

| Column Name | Description |
|-------------|-------------|
| Open        | All open issues with the `community-ops-` label   |
| Todo        | Issues with a due date or high priority   |
| Doing       | Issues that are currently in-flight (these have action items for the Community Operations Manager)   |
| Closed      | Issues that have been closed or completed   |

### How to Use the community-ops Label in GitLab

Please use the `community-ops` label only when there is an action item needed from the Community Operations Manager. 

If you think an issue or MR is a "nice to know" for the Community Operations Manager, feel free to loop them in (@lindsayolson) via a comment instead. 


## Community Operations Response Channels

| Channel Name | Source      | Action.     |
|--------------|-------------|-------------|
| HackerNews    | Hacker News mentions via Zapier   | Find an expert, and collaborate with the Developer Evangelism Team in Slack |
| Hacker News front page stories    | Hacker News mentions via Zapier   | Find an expert, and collaborate with the Developer Evangelism Team in Slack |
| GitLab Forum    | Discourse (forum.gitlab.com)   | Find an expert in the Forum Contributors Group, and collaborate in Slack |
| GitLab Blog    | Disqus (blog.gitlab.com)   | Find an expert (usually the blog post author), and collaborate in Slack |

## Relevant Processes

It is not uncommon that the Social Media Team, and Communications Team at GitLab reaches out to Community Operations. Because of Community Operaions' response channels, and nature of the program, Community Operations has a unique view of the community's sentiment and tone. This means that the Community Operations Manager uses the following resources and handbook pages as needed, when partnering with Social and Comms to ensure a quality response back to our community in times of crisis or gerneral low sentiment and tone.

### Social Media Team

[Social Media Guidelines](https://about.gitlab.com/handbook/marketing/social-media-guidelines/)

[Community Relations Handbook](https://about.gitlab.com/handbook/marketing/community-relations/)

### Expert Responses

Occasionally the Community Operations Manager will encourage GitLab Team Members, as experts, to engage with the wider GitLab community following our Team Member [Social Media Guidelines](/handbook/marketing/social-media-guidelines/) as well as these additional flexible guidelines, listed here:  

1. Experts are encouraged be themselves, while using the [GitLab Writing Style Guidelines](/handbook/communication/#writing-style-guidelines) - we always want to be personable and human in our online presence. 
2. Engage as yourself whenever possible. (ex. use your personal Twitter handle) 
3. Always try and link to relevant documentation, issues, handbook pages, forum topics and other resources.

#### Examples of when to involve an expert

- Technical questions or comments on a GitLab blog post
- HackerNews questions about a strategic GitLab topic (e.g. CI, security) or misinformation about GitLab
- Forum topics re: specific use cases not covered in the docs or relevant product feedback


#### Slack Template for Involving Experts

> @expert_username [LINK TO COMMUNITY COMMENT] Hello! An expert is needed to respond to this. Could you please answer on [name of social platform] using your own individual account?  If you don't know the answer, could you share your thoughts and ping a specific expert who might? Or if there is a more appropriate channel to ask, could you point me in that direction? Thanks!

### Support for Education, Open Source and Startup Programs

Community Operations supports the [Education](/handbook/marketing/community-relations/education-program), [Open Source](/handbook/marketing/community-relations/opensource-program) and [Startups](/solutions/startups) Programs, and their Program Managers.

Their role is to process and manage program applications as per the [application management workflow](/handbook/marketing/community-relations/community-advocacy/workflows/education-oss-startup).
