---
layout: handbook-page-toc
title: GitLab Groundworks Implementation
---


# Implementation of the EdCast LXP


## Go Live Dates and Rolling Wave Plan

GitLab has created a three phase plan for the implementation of the EdCast LXP.

| Phase | Timeline | Audience |
| ----- | ----- | ----- |
| Phase 1 | Q4FY21 | two core audiences: external customers/prospects and community members, and the field certification program (GitLab Account Executives (AEs), Strategic Account Leaders (SALs), Solution Architects (SAs), and Technical Account Managers (TAMs)
| Phase 2 | by start of Q1FY22 | All GitLab team members |
| Phase 3 | Q1FY22 | GitLab Partners migratie from Impartner LMS to EdCast LXP |


## Business Readiness


## Project Management using GitLab

The LXP Implementation team uses [GitLab issue boards](https://gitlab.com/gitlab-com/people-group/learning-development/edcast-lxp-implementation/-/boards) and [GitLab Epics](https://gitlab.com/groups/gitlab-com/-/epics/918) to track and collaborate on the roll out of the EdCast LXP.