---
layout: handbook-page-toc
title: "People Group READMEs"
description: "The READMEs for the People Group team at GitLab can be found on this page."
---

## People Group READMEs

- [April Hoffbauer's README](/handbook/people-group/readmes/ahoffbauer/)
- [Trevor Knudsen's README](/handbook/people-group/readmes/tknudsen)
- [Nadia Vatalidis's README](/handbook/people-group/readmes/nvatalidis)
- [Ashley Jones's README](/handbook/people-group/readmes/asjones/)
