---
layout: handbook-page-toc
title: Crucial Conversations
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


# Crucial Conversations at GitLab

Any synchronous or asynchronous engagement with team members may turn into a crucial conversation. At GitLab, we can develop the skills of sensing the tone of an async or sync conversation to uncover potential pain-points, risks, blockers, etc for team members. We need to find a way to create [psychological safety](/handbook/leadership/emotional-intelligence/psychological-safety/#introduction) for our people. Using a 1-1 can be a great way to gain context on a situation a team member is facing and hold a crucial conversation. 

What is a Crucial Conversation: According to the [book](https://www.amazon.com/Crucial-Conversations-Talking-Stakes-Second/dp/0071771328/ref=sr_1_1?dchild=1&keywords=crucial+conversation&qid=1605712836&sr=8-1), a conversation to be crucial is that the results of it could have a huge impact on the quality of your life. A conversation involving a promotion, performance, debate between coworkers, etc. In short crucial conversations are discussions between two or more people where: 
1. Stakes are high
2. Opinion vary
3. Emotions run strong

Most of us are good at avoiding them because it's human nature to avoid pain and discomfort when addressing a crucial conversation. So what do you do when faced with a crucial conversation?
1. We can avoid them
2. We can face them and them poorly
3. We can them and handle them well

At GitLab, there can be many instances where a crucial conversation is needed. Whether it is addressing the underperformance of a team member, discussing the results of promotion, interviewing, being notified that a high performer is leaving the team, etc. The Crucial Conversations book has strategies laid out that can help master one:
1. Power of Dialogue: Gaining information and context on the situation. The free flow of meaning between two or more people. 
2. Shared Meaning: Each of us enters conversations with our own opinions, feelings, theories, and experiences about the topic at hand. Coming to a shared opinion and entering a pool of a shared meaning helps our synergy with team members in many ways. 
3. Start with the Heart: If you can’t get yourself right, you’ll have a hard time getting dialogue right by looking inward first. Apply empathy in the moment by asking questions to yourself that return you to the dialogue:
   - What do I really want for myself?
   - What do I really want for others?
   - What do I really want for the relationship?
   - How would I behave if I really wanted these results?
4. Learn to Look - How to Notice When Safety is at Risk: It can be difficult to see what exactly is going on and why during crucial conversations. Watch for these situations to learn to spot one: 
   - The moment a conversation turn crucial
   - Signs that people feel safe
   - Your own style under stress
5. Make It Safe: Nothing kills dialogue than fear. When you make it safe, you can talk about almost anything and people will listen. If you spot safety risks as they happen, you can step out of the conversation, build safety, and then find a way to dialogue about almost anything.
6. Master Your Stories: Learn to exert influence over your feelings by slowing down the storytelling process, take a step back, and retrace your path to action one element at a time.

**Below are additional examples and scenarios of Crucial Conversations at GitLab**

## Compensation Review Conversations

### Compensation Review cycles

Check the most up to date information with regards to our [Annual Compensation Review cycle](/handbook/total-rewards/compensation/compensation-review-cycle/#annual-compensation-review-timeline) and our [Catch Up Compensation Review cycle](/handbook/total-rewards/compensation/compensation-review-cycle/#catch-up-compensation-review-timeline) on the [Total Rewards page](/handbook/total-rewards/compensation/compensation-review-cycle).
### Communication recommendations

* **Communicate the increase face-to-face over [Zoom](/handbook/communication/#zoom).** As a manager, this is an opportunity for you to have a conversation with your team member about their increase and the reason behind it.  Having the conversation over Zoom allows for you to have a dialogue with your team member (versus just sharing the number or percentage) and allows you to pick up other information like tone and non-verbal cues which can tell you more about how someone is feeling.

* **Prepare for the call ahead of time.** As a manager, you should have awareness of the following facts about GitLab's compensation principles (please review the handbook's [Global Compensation](/handbook/total-rewards/compensation/) page) and personal details about your team member in BambooHR:
    * Hire date
    * Current compensation (including base salary, variable (where applicable), etc.)
    * Date of last compensation adjustment
    * Performance Factor
    * Location factor changes
    * Benchmark changes
    * % Increase: Please calculate this number from BambooHR to at least the hundredth place rather than using the rounded version available in Compaas, `((FY21 Salary-FY20 Salary)/FY20 Salary)`

* Communicate the adjustment at the beginning of the meeting. You want to give the team member time to ask questions and discuss their compensation adjustment. Avoid trying to rush to communicate at the end of a [1:1 meeting](/handbook/leadership/1-1/).

* Protect the confidentiality of other team members by avoiding saying things like “everyone else received less than you” or “you were the only team member to get a discretionary increase.”

* Avoid blaming others (For example: “I would have given you more but management didn’t approve.”)

* Avoid making future promises (For example: “In the next review, I will get you a large adjustment.”)

### Scenarios (situations and reactions)

**Situations:**

1. **Receiving an increase:**  A team member is receiving an adjustment based on COLA AND/OR location factor AND/OR benchmark changes AND/OR Discretionary:
    * Sample Script:
    * I am so pleased to inform you that you will be getting an adjustment in this year’s comp review. Your increase is based on one or more the following factors (Performance, Location Factor, Benchmark change and Discretionary.  
    * Your salary will be increased to (insert $ amt in local currency) which is a (insert %) adjustment effective, February 1st.
    * Thank you for your hard work and I look forward to continuing to work with you! Do you have any questions?

2. **Not receiving an increase:**   A team member is not receiving an adjustment:
    * Sample Script:
    * As you know, GitLab just went through our annual compensation review. I wanted to inform you that you are not getting a compensation adjustment at this time. This is due to (examples below)...
        * A team member is new to the team and at the market range for your aligned role.  Your performance is good (if applicable), however, we hire new team members at market rate for compensation so we feel that you are compensated accurately for your role at this time. You may be eligible to participate in the [catch up comp review process](/handbook/total-rewards/compensation/compensation-review-cycle/#catch-up-compensation-review).
        * A team member is not receiving because they are above range for their role.
        * A team member needs to improve performance. For questions on specific situations, please work with your People Business Partner.
    * Although informing a team member that they are not getting a compensation adjustment is a tough message to deliver, we are highly recommending that managers have this direct conversation. This is directly aligned with our transparency value. We want everyone to know why they may not have received an adjustment and give them the space to ask questions.

3.  **Other scenarios:** If you have a scenario different from the above, and/or you need help with messaging, please work with your manager or [People Business Partner](/handbook/people-group/#how-to-reach-the-right-member-of-the-people-group).  

**Possible reactions:**

1. A team member is not happy with their adjustment.
    * Listen to your team member's feedback and allow them to express their concerns.
    * Ask open ended questions to allow for the team member to share more (like what were your expectations for an adjustment this year? Why were those your expectations?)
    * Reiterate GitLab’s compensation philosophy. We pay to market based on the compensation calculator and increase compensation annually based on performance.
    * Point your team member to the Compensation Calculator where they can see the range for their role.
    * If your team member is in good standing from a performance perspective, work with them to put together a development plan to help them achieve their goals (e.g. skill development or a promotion).

2. A team member is not happy with their performance factor.
    * Listen to your team member’s feedback and allow them to express their concerns.
    * Refer back to the conversation you had with your team member at the time performance factors were determined and reiterate the justification and feedback that you provided at that time.
    * If your team member is in good standing from a performance perspective, work with them to put together a [development plan](/handbook/people-group/learning-and-development/career-development/) to help them continue to grow. 

**If you need assistance after reviewing the [handbook](/handbook/total-rewards/compensation/), please work directly with your manager or your [People Business Partner](/handbook/people-group/#how-to-reach-the-right-member-of-the-people-group)**


# Career Development Conversations

### Why career development for team members is key to being a great leader

Providing team members with a clear path of advancement within the group, department or division that includes career development opportunities is a win-win-win for team members, managers and GitLab.  Team members who are challenged, engaged and who are actively contributing relevant solutions stay with organizations. An engaged workforce allows the department and divisions to meet their current and future needs for talent and skills and increases performance and decreases regrettable attrition.

Please make sure as a manager you have reviewed the [Career Development Section in the handbook](/handbook/people-group/learning-and-development/career-development/)

There are many reasons that career development conversations are important. Below are a few examples though there are many more reasons.

### Increase team productivity and performance

For managers, having career development conversations offers them a chance to help develop team members which then has an impact on the overall organization.  It's a critical skill for those serious about increasing their teams' productivity and performance.  Development conversations will help the group, department, division and GitLab retain great team members, which at the end of the day also contributes to the manager's own success.

### Build a bench of high contributors

On top of increasing performance, these career development conversations allow managers to build a bench of top talent contributor and create a succession pipeline.  This will help reduce worries about losing top talent and who the team can turn to for critical tasks.  Good managers know that if you have a strong bench, you have a lot less to worry about.

### Be a great leader

When it comes down to, it the best managers or leaders — the ones that team members want to have and are happy continue being part of the team.  These are the managers that take an interest in their team members lives and career.  By learning about what your team members wants, what they strive for and by being able to help them drive to their goals, you will build credibility, trust and respect.  This in turns also contributes to your overall success as a manager.

### Attract, engage and retain top talent

A managers's job is not to just drive to OKRs and performance.  It is also to attract, engage and retain top talent team members and maximize their contribution to the group, department or division.  If you are interested in your team members aspirations and success, you will attract top talent because people will want to work for you.  Once you get great team members on board, you will need a plan to get them and keep them engaged. Career development can be and should be one of the ways you as a manager invest your time and energy to help drive engagement, performance and results.

## Preparing for a team member's performance and development conversation

### Career conversations do not have to be intimidating

Career development conversations are vital for the team members, managers and GitLab's success.  However, not all managers are prepared to address career development due to either lack of experience in having these conversations or a fear that the conversation will be difficult or unpleasant.  Here are a few other reasons that managers may avoid having these conversations:

*  Career development conversations will take up time they do not have and create more work
*  Team members will want different roles or opportunities on an unrealistic timeframe
*  Team members will have aspirations that do not match their current skill level
*  Team members will take conversations about their career as a promise of a promotion
*  Team members will leave after they have developed new skills

Not discounting these concerns, but as a manager it is your job to engage team members in meaningful career development conversations.  

If a team members expectations or timeframe is unrealistic, you can explore having them use their time to find opportunities that will engage and prepare them for when they opportunities arise. If they are ready for promotion but the opportunity is not in scope or plan for the business it is best to be transparent with the team member. Discuss any timeline of if/when a position may be available or if there is no plan currently to add that role. You can also work with the team member to look for internal opportunities like internship or cross collaborative projects to help keep the team member engaged while continuing in their role.  

If a team members expresses career ambitions that are beyond their current skill set, then that is an opportunity to talk about the next level skills and experience that might help prepare them to get them where they want to go. If your department or division has job matrices or competencies this is a good time to review those with the team member. Technical skills are only one part of the team members ability to continue to grow and succeed. If there are issues with attitude and behaviors this is a good time to also share what the team member needs to work on in regards soft skills as part of their career growth and development.

If you are concerned that your career development conversation could be construed as a promise of a promotion, focus the conversation around skills gap development and opportunities.  Try to focus on the things needed now rather than focusing on a specific time frame.

It is possible that some team members may leave for other opportunities after they acquire a new skill set. However, it is even more possible that they will leave if they believe you or GitLab is not interested in their career aspirations or development. Engaging in meaningful career development conversations show that you as a manager care about them personally and their future development. 

## Preparing for the discussion
### Strengths and wins

As a manager part of your job is to learn more about the team members you manage. What are their skills, interests and career goals? The tone of the career development conversation should be transparent, encouraging, curious and show that you are interested and invested in the team member. Spend some time before you have a conversation to think about the following:

* The team members key strengths, skills and wins
* The team members engagement level
* What are opportunities for their development
* Current level of skills in technical or functional areas
* What are their strong leadership competencies or behaviors

Career development conversations shouldn't be done on a whim or just "winging it," but instead these should be thought out and conversations that you prepare for ahead of time. You should come prepare to provide to provide to offer team members feedback, offer suggestions and to ask pertinent questions.

## What questions might team members ask?

Team members want feedback on their strengths, performance and their potential within GitLab. Be prepared to answer the following questions:

* What you say are my top strengths or top skill?
* What skills should I build on?
* Are you aware of any development opportunities that would be helpful for me?
* I see this as my career path, what career path do you see based on my skills?
* Should I be gathering career development feedback from others?

Since you are already having regular [1:1s](/handbook/leadership/1-1/) with your team member you may have ideas regarding your team members development aspirations. Plan ahead and take the time to think about what questions they may ask and come prepared to the conversation.  

## Having the discussion

### Maintain the right frame of mind

Coming to the meeting with an open mind and the willingness to learn more about the team members career development aspirations will set the right tone for the conversation.

A best practice could be having the team member start the meeting by expressing their goals for the conversation. Your job as a manager is to listen and [understand their desires](/handbook/people-group/learning-and-development/career-development/#the-relationship-between-learning-and-development-and-promotion) and help them explore options that may be available for reaching those goals. Try and refrain from interjecting, let the team member finish before you start talking. Try not to be judgemental on what they say, different people will have different career goals and it is important to respect their ideas. However, this can also be a time for you to provide them with feedback, suggestions, recommendations and guidance. It can also be an opportunity for you to connect them with different people within GitLab and additional resource that support their identified career path.

As a manager you should follow up on the goals and activities you both identify. This will show the team member that you do listen and have a vested interest in their future success. Also, career conversations should not be a one-time annual conversation. A best practice is to meet at least quarterly for a check-in. Keep in mind some team members may want to meet more often and some less frequently.

## Be open about your own career goals and development

If asked, share your career goals with your team members. This transparency shows that you are not only willing to hear their career goals, but you are also willing to share your own career path. That could also include discussing with your team members the areas that you have identified to focus on in the future. Be open about questions regarding your own career path and experiences to date. Your team member may be able to take away some valuable lessons that you have learned in your own journey.

## Additional resources 

Below are several recommended articles regarding career development conversations.

* [5 business reasons to put employee career development at the top of your agenda](https://cezannehr.com/hr-blog/2019/03/five-reasons-to-prioritise-development/)
* [Career Development Mentoring Benefits](https://www.insala.com/blog/benefits-of-career-development-mentoring)
* [Why Employee Development is Important, Neglected and can Cost You Talent](https://www.forbes.com/sites/victorlipman/2013/01/29/why-development-planning-is-important-neglected-and-can-cost-you-young-talent/#22df1a8a6f63)
[* If You're Not Helping People Develop, You're Not Management Material](https://hbr.org/2014/01/if-youre-not-helping-people-develop-youre-not-management-material)


# Strategies for Successful Crucial Conversations

## 9 Influencing Strategies

The ability to influence is an essential leadership skill. To influence is to have an impact on the behaviors, attitudes, opinions and choices of your team members and others across GitLab and externally. Influence should not be confused with power or control. It is also not about manipulating others to get your own way. It is about noticing what motivates team members commitment and using that knowledge to leverage performance and results.  

[Leadership](/handbook/leadership/) has sometimes been described as the ability to influence others. An effective leader does not move team members into action by coercion. An effective leaders will articulate the overall vision and goals for the organization. By doing such this can motivate and move team members to action by tapping into their desires and need for success. Positive influence that is properly channeled can also bring about transformation and change for team members, department, division and the company. A leader that exhibits and exerts positive influence in others will build trust and become a true driving force towards transparency, iteration, collaboration and results.

There are many different influencing strategies and in this section we are going to review 9 that leaders can review. Each strategy below will include a definition, example and ways for leaders to develop this strategy.
 

1.  **Empowerment**: Making others feel valued by involving them in decision making and giving them recognition. 
    * Example: Manager invites their team members to a meeting in order to take their inputs on how to improve product quality.
    * Develop: Finding win-win situations, not taking people for granted, not providing unsolicited advice to people and having confidence in the ability of others.
1.  **Interpersonal Awareness**: Being able to recognize and address the concerns of key stakeholders.
    * Example: A manager/leaders offers support to team members by identifying a list of questions that may be asked in meetings with senior leadership.
    * Develop: Awareness of verbal and non verbal cues, putting yourself in another person's shoes and testing your own understanding of the message.
1.  **Negotiating**: Gaing support from others by mutually agreeing on a common set of goals or outcomes.
    * Example: A vendor who is trying to land a big deal may offer their client incentives or a discount provided the size of the order large enough.
    * Develop: Understand the other person's requirements, identify your non-negotiable, role-play wiht a colleague, practive in a low risk real life situation.
1.  **Networking**: Establish and maintain a network of contacts who you may need to influence going forward.
    * Example: A manager spends time with their peers or cross functional partners in different activites (either work or non work related) in order to develop a better bond/relationship with their network.
    * Develop: Take interest in other peoples lives, find common ground, leverage existing relationships and take advantage of existing opportunities.
1.  **Stakeholder awareness**: Identifying the key stakeholder and the ability to gain support when needed.
    * Example: A manager identifies the key stakeholder and decision maker within a supporting organization and regularly meets with them to collaborate.
    * Develop: Keep your ear to the ground to understand what is happening around you, learn by example, identify the decision makers, appreciate other's points of view.
1.  **Shared vision**: Showing others how your ideas support the broader organizational vision.
    * Example: A manager communicates their vision to their team by helping them understand what they can become and the steps needed to achieve the goal.
    * Develop: Always keep the outcomes in sight, speak about the benefits of the approach and get others to participate.
1.  **Impact and Influence**: Choosing the most appropriate time and manner to present your point, always keeping in mind your audience.
    * Example: A manager understands the landscape or demographics of their audience and is thoughtful in their presentation approach.
    * Develop: Study prominent influencers, try different approaches each time, take formal training, model good influencers within the organization.
1.  **Analytical reasoning**: Using analytical reasoning to convince others about your point of view.
    * Example: A team member uses data and market insights to convince their manager about their ideas.
    * Develop: Have more than 1 reason for your idea, clearly lay out the pros and cons, show clarity of thought and structure, spend time on research and case studies. 
1.  **Coercion**: Using threats or pressure tactics to get other to agree to your point or to comply to rules.
    *  Example: A manager makes a decision with no or little input from team and informs team members of actions or consequences if they do not comply. 
    *  Develop: Use it sparingly and not as the first resort, leave no room for ambiguity and do not use as a way to surpress your team member or others.

## Additional resources

Below are additional resources on influencing and leadership for you to review.

* [What Great Leaders Know about Influence](https://www.forbes.com/sites/rebeccanewton/2016/07/27/six-steps-to-increase-your-influence/#5609705a1edd)
* [Influencing Skills: A Key to Leadership Success!](https://www.linkedin.com/pulse/influencing-skills-key-leadership-success-marcia-zidle-ms-bcc/)
* [Influencing Others: A Key Leadership Skill](https://www.ginaabudi.com/influencing-others-a-key-leadership-skill/)
* [The 5 Key Skills of Influential Leaders Within Every Organization](https://www.inspirationaldevelopment.com/5-key-skills-influential-leaders-every-organisation/)
* [Influence and Leadership](http://www.theelementsofpower.com/power-and-influence-blog/influence-and-leadership/)
* [5 Leadership Strategies Proven to Improve Performance on Your Team](https://crestcomleadership.com/2016/12/01/5-leadership-strategies-to-improve-performance-in-your-company/)
* [The 7 Best Books to Improve Influencing Skills](https://www.roffeypark.com/influencing-skills/the-7-best-books-to-improve-influencing-skills/)
* [7 Ways to Build Influence in the Workplace](https://www.inc.com/jayson-demers/7-ways-to-build-influence-in-the-workplace.html)


----

Return to the main [Leadership Toolkit page](/handbook/people-group/leadership-toolkit/).
