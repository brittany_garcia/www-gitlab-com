---
layout: markdown_page
title: Product Direction - Product Analytics
description: "Product Analytics manages a variety of technologies that are important for GitLab's understanding of how our users use our products. Learn more here!"
canonical_path: "/direction/product-analytics/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Vision

Product Analytics provides the necessary frameworks, tooling, and expertise to help us build a better GitLab. GitLab's [single application approach to DevOps](/handbook/product/single-application/) creates a product that is both wide and deep, encompassing a large collection of features used by many teams within an organization, which are compossed of different types of users. This in turn makes it excedingly complex to properly map out and understand how our customers are using the product and gaining value. 

In order to build the best DevOps product we can, and provide the most value for our customers, we need to collect and analyze usage data across the entire platform so we can investigate trends, patterns, and oppritunities. Insights generated from our Product Analytics program enable GitLab to identify the best place to invest people and resources, what categories to push to maturity faster, where our UI experience can be improved and how product changes effect the business. 

## Guiding Principals 

We understand that usage tracking is a sensitive subject, and we respect and acknowledge our customer's concerns around what we track, how we track it, and what we use it for. To protect and empower our users the Product Analytics team follows these guiding principles:

**Transparency**

We will always be public about what we track and provide documentation around each item.

**User Access and Control**

We wil provide a simple and easily accessable Privacy Control Center, where users can view what they have opted-in to, what is being tracked, and update privacy settings. 

## Top Priorities and Deliverables

Product Analytics sits in the middle of many projects, iniatiives and OKRs at GitLab. In order to provide clarity and realistic expectations to our stakeholders and customers we practive ruthless prioritization([per Product Principal #6](https://about.gitlab.com/handbook/product/product-principles/)), identifying what is above the line, what is below, and what is unfunded and not possible for us to action on in a given timeline. 

### Product Performance Indicators

Product Analytics top priority is to build out the infratructure and analytics frameworks for our Product and Engineering Teams to instrument telemtry through out the GitLab platform and provide guidance on best pracrtices and enforce standardization.  

Teams Involved: Product, Product Analytics, Data

**Product Analytics Responsibilities:** 

- Instrumentation 
- Sisense Dashboarding (Out sourced to Data Team at the moment due to lack of analyst on team)
- Performance Indicators
- Metrics Reviews
- Product Improvements

**FY21-Q4 OKR Support**

In order to support [100% of DevOps groups have Predicted GMAU (or Paid GMAU)](https://gitlab.com/gitlab-com/Product/-/issues/1610) we are working with the PMs of each group that still needs to finalize their GMAU metrics so the Data Team can apply the Predicted GMAU formula to their dashboards. 

- [Create : Static Site Editior GMAU Instrumentation](https://gitlab.com/gitlab-org/gitlab/-/issues/233994)
- [Create : Ecosystem GMAU Instrumentation](https://gitlab.com/gitlab-org/gitlab/-/issues/225678)
- [Geo : Enablement GMAU Instumentation](https://gitlab.com/groups/gitlab-org/-/epics/4660)
- [Verify : Testing GMAU Instrumentation](https://gitlab.com/groups/gitlab-org/-/epics/2991)
- Secure : Fuzz Testing - Confirming instrumentation is functioning properly
- Secire : Threat Insights - Confirming instrumentation is functioning properly

**Why This Is Important:**

Supporting Product Perfomance Indicators empowers our R&D Teams to be data driven and to demonstrate what value they are adding to the platform. These indicators inform the business on where to invest and what customers are leveraging. 

**Recent Progress:**

- FY21-KR1 Delivered: (EVP, Product) [100% of groups have Future GMAU (or Paid GMAU) with instrumentation, dashboards, and quarterly targets](https://gitlab.com/gitlab-com/Product/-/issues/1342)
- FY21-KR2 Delivered: (EVP, Product) [100% of stages have Future SMAU and Paid SMAU with instrumentation, dashboards, and quarterly targets](https://gitlab.com/gitlab-com/Product/-/issues/1343) 
- All [stage and group performance indicators](https://about.gitlab.com/handbook/product/stage-and-group-performance-indicators/) have been added to the handbook. 
- Process defined for [implementing product performance indicators](https://about.gitlab.com/handbook/product/product-analytics-guide/#implementing-product-performance-indicators)

**What's Next:**

- Guide instrumentation of remaining PIs - work with [stages and groups](https://about.gitlab.com/handbook/product/stage-and-group-performance-indicators/) to implement their metrics
- Intersect and union across PIs - allow the ability to combine multiple metrics and PIs together.
- Define metrics review process - align sections on monthly review process.
- Improve data quality of PIs - our first pass at performance indicators needs to be continuously alongside of our collection framework and data infrastructure.

### Support Sales/Customer Success via Project Compass

Teams Involved: Sales, Customer Success, Product, Product Analytics, Data

**Product Analytics Responsibilities:**

- dbt Base Data Models
- Product Data Models
- Customer Success Data Models
- Enterprise Dimensionsal Models
- Snowflake EDW to Salesforce Data Pump
- Salesforce Dashboards
- Salesforce to Gainsight Data Feed 
- Gainsight Dashboards
- Customer Conversations

We are working with Sales and Customer Success to [document the metrics they need](https://docs.google.com/spreadsheets/d/1ZR7duYmjQ8x86iAJ1dCix88GTtPlOyNwiMgeG_85NiA/edit#gid=0) so we can map out a path to delivering them over time.

**Why This Is Important:**

- Supporting Project Compass is crtical to GitLab as it increases our sales & marketing efficiencies andempowers us to react to market and customer trends faster.

**Recent Progress:**

- [Versions App Reporting has been replicated in Sisense Dashboards](https://gitlab.com/gitlab-data/analytics/-/issues/4488/)
- A datafeed has been setup from Sisense to Salesforce
- License data has been added to this data feed

**What's Next:**

- Customer Success Data Models - create SCM, CI, and DevSecOps use cases.
- Tie Product Usage Data to Fulfillment Data - tie together our Usage Pings with license and customer information.
- SaaS Group/Account Level Product Usage + SaaS Product Data Models	- feed SaaS account level data into Salesforce and Gainsight

## Our Roadmap (As of 11/12/2020)

### Resolve Data Integrity Concerns

Teams Involved: Product, Product Analytics, Data, Infrastructure, Sales, Customer Success

**Product Analytics Responsibilities:**

- TBD

**Why This Is Important:**

As GitLab has grown, and our analytics tracking has expanded we are finding areas where the integrity and accuracy of our data needs to be reviewed. Product Analytics is working with internal stakeholders and teams to map out and identify [these areas](https://docs.google.com/spreadsheets/d/1bqgEweYgSI99uK5CoViH_U0NAKOuaQtR4xPXLFBspoE/edit#gid=0) so we can formulate resolutions.

**Recent Progress:**
 
 - Kicked Off investigation and documentation 
 - TBD

 **What's Next:**

 - Link and Create issues for each integrity issue to a globa epic for Product Analytics to champion

### Privacy Policy and Settings - Rollout Updated Privacy Policy

Teams Involved: Legal, Product Analytics, Data, Security

**Product Analytics Responsibilities:**

- Privacy Policy updates
- Internal and External Communication 
- Consolidate Privacy Settings into a single location in the Product
- Expand user control over analytics settings

**Why This Is Important:**

- Our [privacy policy](https://about.gitlab.com/privacy/) has not been updated since the [October 29, 2019 Update on free software and telemetry](https://about.gitlab.com/blog/2019/10/10/update-free-software-and-telemetry/). This policy needs to be updated to align with GitLab's strategy and our Product Analytics direction.
- Our customers and users provided significant feedback on this change and we are working to respect and action against it while supporting our business needs.

**Recent Progress:**

- Draft privacy policy is in place and being reviewed
- We're working on resourcing and designing a Privacy Control Center

**What's Next:**

- [Privacy Policy](https://gitlab.com/groups/gitlab-com/-/epics/907) - work through draft privacy policies and product analytics policies, gather internal and community feedback, create a communications and rollout plan.
- Privacy Control Center - we're building a centralized privacy settings page in GitLab for users to easily understand and control their privacy settings.

### Data Collection - Maintain and Scale Usage Ping

Teams Involved: Product Analytics, Data, Product Managers

**Product Analytics Responsibilities:**

- Expand Collection Framework
- Maintain Event Dictionary
- Expand and Guide Instrumentation
- Optimize and Maintain Usage Ping Generation
- Maintain and Scale Usage Ping Collector	

**Why This Is Important:**

- Product Metrics are currently instrumented by all product teams to track key metrics of usage. We're close to 3x the number of metrics tracked since the beginning of this 2020, as such, we need to ensure metrics are added in a structured and standardized way. Usage Ping on GitLab.com takes over 32 hours to generate at this time, which is 4x longer than at the beginning of this year. In order for all this instrumentation to be as valuable as possible we need to reduce the amount of time it takes to generate and be accessible by the business.

**Recent Progress:**

- Over 800 metrics tracked in Usage Ping
- Product Analytics review process in place
- Usage Ping and Snowplow documentation
- Client and server-side counts can now be tracked on SaaS and Self-Managed.
- Client and server-side events are a work in progress on Self-Managed.
- Plan-level reporting using SaaS Usage Ping is not possible as SaaS is multi-tenant and Usage Ping reports at an Instance level.
- We've implemented an [Event Dictionary for Usage Ping](https://gitlab.com/groups/gitlab-org/-/epics/4174)

**What's Next:**

- Usage Ping Standardizations and Data Integrity - create a standardized naming convention, define usage ping metrics in a centralized YAML file, generate the event dictionary automatically from this file.
- Usage Ping Parallelization - compute usage ping metrics in parallel instead of serially. 
- Usage Ping Collector - build out a versioned Usage Ping collector endpoint to collect restructured Usage Ping data.

### Data Collection - Enable Snowplow Event Tracking

Teams Involved: Product Analytics, Data, Infrastructure, Product

**Product Analytics Responsibilities:** 

- Expand Collection Framework
- Maintain Event Dictionary
- Expand and Guide Instrumentation
- Support Product Usage Analysis
- Maintain and Expand Snowplow Collector
- Maintain and Scale Snowplow Enricher

**Why This Is Important:**

Snowplow allows us to collect a richer set of usage data, build more complex data modeling and usage funnels, and gain a deeper understanding of user actions and paths through the product. 

**Recent Progress:**

- Proof on concept for Product Analyitcs Snowplow and Usage Ping built.

**What's Next:**

- Snowplow Tooling, Standardizations, and Linking with Usage Ping - Standardize Snowplow schemas and instrumentation methods, create an Event Dictioanry for Snowplow, build out development and testing tools along with documentation. Finish tying Usage Ping and Product Analytic's Snowplow together specifically merging tracking classes into a single class with multiple destinations.
- Scale Product Analytics - Take ownership of our Snowplow Infrastructure and maintain the snowplow collector and enricher. Scale Product Analytics's internal database and collector while working towards supporting an external database and collector

###Processing Pipeline - Decrease Cycle Times for Product Analytics

Teams Involved: Product Analytics, Data, Infrastructure, Product

**Product Analytics Responsibilities:** 

- Release Cycle
- Product Usage
- Usage Ping Generation
- Usage Ping Collector
- Extractors
- Loaders
- Snowflake Data Warehouse

**Why This Is Important:**

The Data Availability cycle times are currently a 51 day cycle and our exports of the Versions DB are currently done manually every 14 days according to [this schedule](https://gitlab.com/groups/gitlab-data/-/epics/162)  In order for all this instrumentation to be as valuable as possible we need to reduce the amount of time it takes to generate and be accessible by the business. 

**Recent Progress:**

- Proof of concept built for exporting data from Versions application using CI pipelines.

**What's Next:**

- [Automate Versions DB Exports](https://gitlab.com/gitlab-org/product-analytics/-/issues/398) - currently Usage Pings are stored in the Versions DB and this is manually exported into the data warehouse every 14 days. This is a manual process by both the infrastructure and data engineering teams. This will allow us reduce import times from 14 days to 1 day.	
- Generate Usage Ping Daily - currently Usage Pings are generated every 7 days, this change will make generating and sending Usage Ping every day. This allows us to reduce generation times from 7 days to 1 day.	

### Processing Pipeline - Plan and Group-level Reporting for SaaS

Teams Involved: Product Analytics, Data 

**Product Analytics Responsibilities:** 
- Collection Framework
- Usage Ping Generation
- Collectors
- Processors
- Snowflake Data Warehouse
- dbt Data Model

**Why This Is Important:**

Currently Usage Ping is not segmented by pricing tier, which means for any SaaS free / paid account segmentation cannot be done using Usage Ping. Instead, as a work around, the data team is using the Postgres database import and manually recreating all Usage Ping queries in Sisense. A single usage ping on SaaS takes 32+ hours to generate, for group level metrics, we need to run this 1 million times which is only feasible if it's done in the data warehouse.

**Recent Progress:**

- [Usage Ping SQL queries exported](https://gitlab.com/gitlab-org/product-analytics/-/issues/423)

**What's Next:**

- Export Usage Ping Queries - export SQL queries for all Postgres based metrics, find a way to export non-SQL queries for Usage Ping metrics in Redis, integrations, settings.
- Data Modelling and Scheduling - setup the Snowflake EDW to run Usage Ping query workload, build dbt Base Data Models and Product Data Models, setup transformation pipeline schedule.

### Product Performance Indicators

Product Analytics Steps: Instrumentation, Sisense Dashboard, Performance Indicators, Metrics Reviews, Product Improvements

Teams Involved: Product Managers, Product Analytics, Data

**Why This Is Important:**

- Product performance indicators help guide each of our product sections, stages, and groups to be metrics driven.

**Recent Progress:**

- FY21-KR1 Delivered: (EVP, Product) [100% of groups have Future GMAU (or Paid GMAU) with instrumentation, dashboards, and quarterly targets](https://gitlab.com/gitlab-com/Product/-/issues/1342)
- FY21-KR2 Delivered: (EVP, Product) [100% of stages have Future SMAU and Paid SMAU with instrumentation, dashboards, and quarterly targets](https://gitlab.com/gitlab-com/Product/-/issues/1343) 
- All [stage and group performance indicators](https://about.gitlab.com/handbook/product/stage-and-group-performance-indicators/) have been added to the handbook. 
- Process defined for [implementing product performance indicators](https://about.gitlab.com/handbook/product/product-analytics-guide/#implementing-product-performance-indicators)

**What's Next:**

- Guide instrumentation of remaining PIs - work with [stages and groups](https://about.gitlab.com/handbook/product/stage-and-group-performance-indicators/) to implement their metrics
- Intersect and union across PIs - allow the ability to combine multiple metrics and PIs together.
- Define metrics review process - align sections on monthly review process.
- Improve data quality of PIs - our first pass at performance indicators needs to be continuously alongside of our collection framework and data infrastructure.

### Project Compass

Product Analytics Steps: dbt Base Data Models, Product Data Models, Customer Success Data Models, Enterprise Dimensionsal Models, Snowflake EDW to Salesforce Data Pump, Salesforce Dashboards, Salesforce to Gainsight Data Feed, Gainsight Dashboards, Customer Conversations

Teams Involved: Sales, Customer Success, Product Analytics, Data

**Why This Is Important:**

- Project Compass is important as it increases our sales & marketing efficiencies.

**Recent Progress:**

- [Versions App Reporting has been replicated in Sisense Dashboards](https://gitlab.com/gitlab-data/analytics/-/issues/4488/)
- A datafeed has been setup from Sisense to Salesforce
- License data has been added to this data feed

**What's Next:**

- Customer Success Data Models - create SCM, CI, and DevSecOps use cases.
- Tie Product Usage Data to Fulfillment Data - tie together our Usage Pings with license and customer information.
- SaaS Group/Account Level Product Usage + SaaS Product Data Models	- feed SaaS account level data into Salesforce and Gainsight

## Quick Links

| Resource                                                                                                                          | Description                                               |
|-----------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------|
| [Product Analytics Guide](/handbook/product/product-analytics-guide)                                                                              | A guide to Product Analytics                   |
| [Usage Ping Guide](https://docs.gitlab.com/ee/development/product_analytics/usage_ping.html)                                              | An implementation guide for Usage Ping                    |
| [Snowplow Guide](https://docs.gitlab.com/ee/development/product_analytics/snowplow.html)                                                  | An implementation guide for Snowplow                      |
| [Event Dictionary](/handbook/product/product-analytics-guide#event-dictionary)                                        | A SSoT for all collected metrics and events               |
| [Privacy Policy](/privacy/)                                                                                                       | Our privacy policy outlining what data we collect and how we handle it     |
| [Implementing Product Performance Indicators](/handbook/product/product-analytics-guide#implementing-product-performance-indicators)                                   | The workflow for putting product performance indicators in place   |
| [Product Analytics Direction](/direction/product-analytics/)                                                                              | The roadmap for Product Analytics at GitLab                       |
| [Product Analytics Development Process](/handbook/engineering/development/growth/product-analytics/) | The development process for the Product Analytics groups         |
